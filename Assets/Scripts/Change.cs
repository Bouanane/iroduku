﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Change : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite newSprite;

    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            ChangeSprite();
        }
        
    }
    void ChangeSprite()
    {
        spriteRenderer.sprite = newSprite; 
    }
 
}
