﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowUp : MonoBehaviour
{
    private Vector3 mousePosition;
    public float moveSpeed;
    public GameObject Mask;
    public GameObject sc;
    public GameObject one;
    public GameObject two;
    public GameObject thr;
    public GameObject four;
    public GameObject five;
    public Animator anime;
    public GameObject col;
    public GameObject colp;
 
    
    // Use this for initialization
    void Start () {
        
    }
   
    // Update is called once per frame
    void Update () {
        if (Input.GetMouseButton(1)) {
            Mask.SetActive(true);
            mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            transform.position = Vector2.Lerp(transform.position, mousePosition, moveSpeed);
        } 

        if (Input.GetKey(KeyCode.A))
        {
            float scale=15.0f;
            sc.transform.localScale = new Vector2(5,5);
            scale++;
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            one.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            two.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            thr.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            four.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            col.SetActive(false);
            colp.SetActive(true);
            anime.SetBool("THIC",true);
            five.SetActive(true);
        }

    }
    
}
